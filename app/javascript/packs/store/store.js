import axios from 'axios'

const state = {
  todo_input: '',
  todos: ''
}

const mutations = {
  setTodos(state, todos){
      state.todos = todos
  },
  setTodoInput(state, todoInput){
      state.todo_input = todoInput
  }
}

const actions = {
  fetchTodos(context) {
    axios.get('./api/v1/todos')
      .then(res => {
        context.commit('setTodos', res.data)
      })
      .catch(error => {
        throw error
      })
  },
  postTodo(context, todo) {
    axios.post('/api/v1/todos', todo)
      .then(res => {
        context.commit('setTodoInput', ''),
        this.fetchTodos()
      })
      .catch(error => {
        throw error
      })
  },
  complete(context, todoId, todoUpdate) {
    axios.put('/api/v1/todos/' + todoId, todoUpdate)
      .then(res => {
        this.fetchTodos()
      })
      .catch(error => {
        throw error
      })
  },
  deleteTodo(todoItem) {
    axios.delete('/api/v1/todos/' + todoItem.id, { "todo": 
      { "completed": !todoItem.completed } })
      .then(res => {
        this.fetchTodos()
      })
      .catch(error => {
        throw error
      })
  }
}

export default {
  state,
  mutations,
  actions
}