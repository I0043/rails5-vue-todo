import Vue from 'vue'
import App from './todo_app.vue'

import Vuex from 'vuex'
import _store from './store'

Vue.use(Vuex);

const store = new Vuex.Store(_store)

document.addEventListener('DOMContentLoaded', () => {
  document.body.appendChild(document.createElement('todo'))
  const app = new Vue({
    el: 'todo',
    template: '<App/>',
    components: { App },
    store
  })
  console.log(app)
})